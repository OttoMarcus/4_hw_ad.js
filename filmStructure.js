//  structure for film details

const container = document.querySelector(".container")

export default function filmStructure(arg, id) {
    let article = document.createElement("div");
    article.classList.add("article");
    article.innerHTML = `<span>${arg}</span>`;
    container.appendChild(article);
  
  // create sub Ul into film item
    let ulUnderLi = document.createElement("ul");
    ulUnderLi.classList.add(`film-name-${id}`);
    article.appendChild(ulUnderLi);
  }
  