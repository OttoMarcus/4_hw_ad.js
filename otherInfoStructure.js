//  structure for episodes, resume and actors
export default function otherInfoStructure(id, list) {
    const entryPoint = document.querySelector(`.film-name-${id}`);
    list.forEach(el => {
         let li = document.createElement("li");
         li.classList.add(`infoByFilm-${id}`);
         li.textContent = el;
         entryPoint.appendChild(li);
    });
}
