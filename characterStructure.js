//structure for actors
export default function characterStructure(id, arrOfCharacters) {
    const entryPoint = document.querySelector(`.infoByFilm-${id}`);
    let ul = document.createElement("ul");
    ul.classList.add(`charactersList-${id}`);
    entryPoint.appendChild(ul);
    for (const character of arrOfCharacters) {
        let li = document.createElement("li");
        li.textContent = `- ${ character}`;
        ul.appendChild(li);
    }
}