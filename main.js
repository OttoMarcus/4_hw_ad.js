

//  structure for film details
import filmStructure from "./filmStructure.js";
import otherInfoStructure from "./otherInfoStructure.js";
import characterStructure from "./characterStructure.js";

async function filmList() {
  try {
    const response = await fetch("https://ajax.test-danit.com/api/swapi/films");
    const data = await response.json();

    for (const element of data) {
      const episodeId = `Episode: ${element.episodeId}`;
      const characters = "Characters:";
      const openingCrawl = `Resume: ${element.openingCrawl}`;
      
      // Fetch and extract character names
      const characterNames = await fetchCharactersNames(element);

      const list = [characters, episodeId, openingCrawl ];
      filmStructure(element.name, element.id);
      otherInfoStructure(element.id, list);
      characterStructure(element.id, characterNames);
    }
  } catch (error) {
    console.error("Error fetching film list:", error);
  }
}

// Extract names of actors
async function fetchCharactersNames(obj) {
  try {
    const characterUrls = obj.characters;

    // Fetch each character API and extract the "name" element
    const characterNamesPromises = characterUrls.map(async (characterUrl) => {
      const characterResponse = await fetch(characterUrl);
      const characterData = await characterResponse.json();
// console.log(characterData);
      return characterData.name;
    });
    // Wait for all promises to resolve and get the names
    const characterNames = await Promise.all(characterNamesPromises);

    return characterNames;
  }
   catch (error) {
    console.error("Error fetching characters names:", error);
  }
}


filmList();